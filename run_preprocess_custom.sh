CUSTOM_DATA_DIR=/media/apply/videoDisk/new_model/
TRAIN_DIR=/raw_data/train/
VAL_DIR=/raw_data/validation/
OUTPUT_DIR=/output/
# build the preprocessing script.
bazel build inception/preprocess_custom_model

# run it
bazel-bin/inception/preprocess_custom_model "${CUSTOM_DATA_DIR}"


cd "${CURRENT_DIR}"
BUILD_SCRIPT="inception/build_image_data"

echo "building script: inception/build_image_data"
OUTPUT_DIRECTORY="${DATA_DIR}"
"${BUILD_SCRIPT}" \

bazel-bin/inception/build_image_data \
  --train_directory="${CUSTOM_DATA_DIR}${TRAIN_DIR}" \
  --validation_directory="${CUSTOM_DATA_DIR}${VAL_DIR}" \
  --output_directory="${CUSTOM_DATA_DIR}${OUTPUT_DIR}" \
  --labels_file="${CUSTOM_DATA_DIR}/raw_data/labels.txt"
