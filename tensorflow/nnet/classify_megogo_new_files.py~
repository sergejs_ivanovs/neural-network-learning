# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Simple image classification with Inception.

Run image classification with Inception trained on ImageNet 2012 Challenge data
set.

This program creates a graph from a saved GraphDef protocol buffer,
and runs inference on an input JPEG image. It outputs human readable
strings of the top 5 predictions along with their probabilities.

Change the --image_file argument to any jpg image to compute a
classification of that image.

Please see the tutorial and website for a detailed description of how
to use this script to perform image recognition.

https://tensorflow.org/tutorials/image_recognition/
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os.path
import re
import sys
import tarfile
import MySQLdb
import urllib as urllb
import simplejson as json
import urllib2
import gc
import threading
import time
from collections import deque
from decimal import Decimal
import atexit
import signal
from PIL import Image
import requests
from StringIO import StringIO
import base64
#import proc

import time
import cv2

# pylint: disable=unused-import,g-bad-import-order
import tensorflow.python.platform
from six.moves import urllib
import numpy as np
import tensorflow as tf
from datetime import datetime


FFMPEG_BIN = "ffmpeg"
import subprocess as sp


    
# pylint: enable=unused-import,g-bad-import-order

from tensorflow.python.platform import gfile

FLAGS = tf.app.flags.FLAGS

db_host = "localhost"
db_user = "root"
db_passwd="1password!"
db_structure = "test"

database = MySQLdb.connect(host=db_host, user=db_user,passwd=db_passwd,db=db_structure)
images = deque([])
run_image_loading = True
image_buffer = 35
lock = threading.Lock()
sess = tf.Session()
t1_stop = threading.Event()


#vidcap = cv2.VideoCapture('/home/sergejs/megogo/8dates.mp4')


# classify_image_graph_def.pb:
#   Binary representation of the GraphDef protocol buffer.
# imagenet_synset_to_human_label_map.txt:
#   Map from synset ID to a human readable string.
# imagenet_2012_challenge_label_map_proto.pbtxt:
#   Text representation of a protocol buffer mapping a label to synset ID.
tf.app.flags.DEFINE_string(
    'model_dir', '/tmp/imagenet',
    """Path to classify_image_graph_def.pb, """
    """imagenet_synset_to_human_label_map.txt, and """
    """imagenet_2012_challenge_label_map_proto.pbtxt.""")
tf.app.flags.DEFINE_string('image_file', '',
                           """Absolute path to image file.""")
tf.app.flags.DEFINE_integer('num_top_predictions', 5,
                            """Display this many predictions.""")

# pylint: disable=line-too-long
DATA_URL = 'http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz'
# pylint: enable=line-too-long


class NodeLookup(object):
  """Converts integer node ID's to human readable labels."""

  def __init__(self,
               label_lookup_path=None,
               uid_lookup_path=None):
    if not label_lookup_path:
      label_lookup_path = os.path.join(
          FLAGS.model_dir, 'imagenet_2012_challenge_label_map_proto.pbtxt')
    if not uid_lookup_path:
      uid_lookup_path = os.path.join(
          FLAGS.model_dir, 'imagenet_synset_to_human_label_map.txt')
    self.node_lookup = self.load(label_lookup_path, uid_lookup_path)

  def load(self, label_lookup_path, uid_lookup_path):
    """Loads a human readable English name for each softmax node.

    Args:
      label_lookup_path: string UID to integer node ID.
      uid_lookup_path: string UID to human-readable string.

    Returns:
      dict from integer node ID to human-readable string.
    """
    if not gfile.Exists(uid_lookup_path):
      tf.logging.fatal('File does not exist %s', uid_lookup_path)
    if not gfile.Exists(label_lookup_path):
      tf.logging.fatal('File does not exist %s', label_lookup_path)

    # Loads mapping from string UID to human-readable string
    proto_as_ascii_lines = gfile.GFile(uid_lookup_path).readlines()
    uid_to_human = {}
    p = re.compile(r'[n\d]*[ \S,]*')
    for line in proto_as_ascii_lines:
      parsed_items = p.findall(line)
      uid = parsed_items[0]
      human_string = parsed_items[2]
      uid_to_human[uid] = human_string

    # Loads mapping from string UID to integer node ID.
    node_id_to_uid = {}
    proto_as_ascii = gfile.GFile(label_lookup_path).readlines()
    for line in proto_as_ascii:
      if line.startswith('  target_class:'):
        target_class = int(line.split(': ')[1])
      if line.startswith('  target_class_string:'):
        target_class_string = line.split(': ')[1]
        node_id_to_uid[target_class] = target_class_string[1:-2]

    # Loads the final mapping of integer node ID to human-readable string
    node_id_to_name = {}
    for key, val in node_id_to_uid.items():
      if val not in uid_to_human:
        tf.logging.fatal('Failed to locate: %s', val)
      name = uid_to_human[val]
      node_id_to_name[key] = name

    return node_id_to_name

  def id_to_string(self, node_id):
    if node_id not in self.node_lookup:
      return ''
    return self.node_lookup[node_id]


def create_graph():
  """"Creates a graph from saved GraphDef file and returns a saver."""
  # Creates graph from saved graph_def.pb.
  with gfile.FastGFile(os.path.join(
      FLAGS.model_dir, 'classify_image_graph_def.pb'), 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    _ = tf.import_graph_def(graph_def, name='')

def file_exists(location):
    request = urllib2.Request(location)
    request.get_method = lambda : 'HEAD'
    try:
        urllib2.urlopen(request)
        return True
    except urllib2.HTTPError:
        return False

def get_rescaled_images(image, scale_step, min_scale, max_scale):
    result = []
    result.append((image, 1.0))
    
    height, width, channels = image.shape

    scale = 1 - scale_step;    
    while scale >= min_scale:
        print(scale)
        if (scale*height < 299):
            scale = 299/height
            min_scale = scale
        imgSmall = cv2.resize(image, (0,0), fx = scale, fy = scale)
        result.append((imgSmall, scale))
        scale -= scale_step
    scale = 1 + scale_step
    while scale <= max_scale:
        print(scale)
        imgBig = cv2.resize(image, (0,0), fx = scale, fy = scale)
        result.append((imgBig, scale))
        scale += scale_step
    print("got rescaled images: " , len(result))
    return result
    
def get_sliding_window_images(image, scale, step = 200, size = 299):
    height, width, channels = image.shape
    print("input image size",str(height), str(width))
   
    rez = []
    
    pointStartX = 0
    pointStartY = 0
    wWidth = size
    wHeight = size

    
    gapX = (width-size)%step
    gapY = (height-size)%step
 
    stepCountX = int((width-size)/step) +1
    stepCountY = int((height-size)/step) +1
    
    if stepCountX == 1:
        stepCorrX = step-gapX
        stepX = step - stepCorrX
    elif stepCountX == 0:
        stepCorrX = 0
        stepX = step
    else:
        stepCorrX = (gapX)/(stepCountX-1)
        stepX = step + stepCorrX
    
    if stepCountY == 1:
        stepCorrY = step-gapY
        stepY = step - stepCorrY
    elif stepCountY == 0:
        stepCorrY = 0
        stepY = step
    else:
        stepCorrY = (gapY)/(stepCountY-1)
        stepY = step + stepCorrY
    
#    print("SLIDING STEPS", stepX, stepY, gapX, gapY, stepCountX, stepCountY, stepCorrX, stepCorrY, step)
    
    if stepX == 0:
        stepX = step
    if stepY == 0:
        stepY = step
    while pointStartX + wWidth <= width:
        while pointStartY + wHeight <= height:
            crop_img = image[int(pointStartY):int(pointStartY+wHeight), int(pointStartX):int(pointStartX+wWidth)]
            rez.append((crop_img, int(pointStartX/scale), int(pointStartY/scale), int(wWidth/scale), int(wHeight/scale)))       
            pointStartY += stepY
            
        pointStartX += stepX
        pointStartY = 0
    return rez
        


def run_inference_on_image(image_data):
    global sess
    data =  []
#    cv2.imwrite("image.jpg", image_data)
#    cv2.imshow('image', image_data)
#    cv2.waitKey()
    full_images = get_rescaled_images(image_data, 0.5, 0.5, 1.5)
    
    print("full_images", len(full_images))    
    
    for fimage, scale in full_images:
        images = get_sliding_window_images(fimage,scale)
        print("smallImageSize" , len(images))
#        images.append(image_data)
        print(len(images))
        for image, X, Y, width, height in images:
              
            # Some useful tensors:
            # 'softmax:0': A tensor containing the normalized prediction across
            #   1000 labels.
            # 'pool_3:0': A tensor containing the next-to-last layer containing 2048
            #   float description of the image.
            # 'DecodeJpeg/contents:0': A tensor containing a string providing JPEG
            #   encoding of the image.
            # Runs the softmax tensor by feeding the image_data as input to the graph.
           
#            softmax_tensor = sess.graph.get_tensor_by_name('softmax:0')
#            np_image2 = np.asarray(image)
#            np_image3 = cv2.imencode('.jpg', np_image2)[1].tostring()
#            predictions = sess.run(softmax_tensor,  {'DecodeJpeg/contents:0': np_image3})
            try:
                softmax_tensor = sess.graph.get_tensor_by_name('softmax:0')
                

                
                np_image2 = np.asarray(image)
                np_image3 = cv2.imencode('.jpg', np_image2)[1].tostring()
                predictions = sess.run(softmax_tensor,  {'DecodeJpeg/contents:0': np_image3})

            except:
                print("Failed to read image_data to tensor")
                return ""
                
            predictions = np.squeeze(predictions)
            
            # Creates node ID --> English string lookup.
            node_lookup = NodeLookup()
            
            top_k = predictions.argsort()[-FLAGS.num_top_predictions:][::-1]
            
            for node_id in top_k:
                  human_string = node_lookup.id_to_string(node_id)
                  score = predictions[node_id]
                  data.append((human_string, Decimal(str(score)), X, Y, width, height))
#                  print('%s (score = %.5f)' % (human_string, score))

    rezData = []
    for d in data:
        if (d[1] > 0.15):
            rezData.append(d)
            
    temp = {}
    for key, precision,X, Y, width, height in rezData:
        if key not in temp: # we see this key for the first time
            temp[key] = (key, precision, X, Y, width, height)
        else:
            if temp[key][1] < precision: # the new date is larger than the old one
                temp[key] = (key, precision, X, Y, width, height)
    rezData = temp.values()
    
    rezData.sort(key=lambda tup: tup[1], reverse=True)    
    rezData = rezData[:-len(rezData)+10]
#    print(data)
    
    rezJson = json.dumps(rezData)
    gc.collect()
    print(rezJson)
#    for key, precision,X, Y, width, height in rezData:
#        if(precision> 0.8):
#            cv2.rectangle(image_data, (X, Y), (X+width, Y+height), (255,0,0), 2)
#
#    cv2.imshow('image', image_data)
#    cv2.waitKey()
    return rezJson


def maybe_download_and_extract():
  """Download and extract model tar file."""
  dest_directory = FLAGS.model_dir
  if not os.path.exists(dest_directory):
    os.makedirs(dest_directory)
  filename = DATA_URL.split('/')[-1]
  filepath = os.path.join(dest_directory, filename)
  if not os.path.exists(filepath):
    def _progress(count, block_size, total_size):
      sys.stdout.write('\r>> Downloading %s %.1f%%' % (
          filename, float(count * block_size) / float(total_size) * 100.0))
      sys.stdout.flush()
    filepath, _ = urllib.request.urlretrieve(DATA_URL, filepath,
                                             reporthook=_progress)
    print()
    statinfo = os.stat(filepath)
    print('Succesfully downloaded', filename, statinfo.st_size, 'bytes.')
  tarfile.open(filepath, 'r:gz').extractall(dest_directory)

def disconnect_from_db():
    database.close()

def get_path(date, source):
    if(type(date) is datetime):
        return "http://"+str(db_host)+":"+str(image_port)+"/dvr/"+str(source)+"/"+date.strftime("%Y.%m.%d/%H.%M.%S")+"/00_n.jpg"
    return False
    
def get_rows(limit=100):
    
    query1 = "SELECT * FROM `ContextTmp` WHERE `processed` = 0 ORDER BY ts ASC LIMIT " + str(limit)
    cursor = execute_query(query1)
    if cursor == "query failed":
        return []
    results = cursor.fetchall()
    query2 = "UPDATE `ContextTmp` SET `processed`= 3 WHERE id IN ("
    for row in results:
        query2+= str(row[0])+", "
    query2 = query2[:-2]
    query2 += ")"
    execute_query(query2)
    
    return results
    
def write_result(jsonObject, time, name):
    jsonObject = re.sub("((?<![(,])'(?![,)]))", "''", str(jsonObject))
    query = "INSERT INTO `megogoNeur` (`ts`, `tags`, `name`) VALUES ('"+ str(time) +"', '"+ jsonObject +"', '"+ name+"')"
    execute_query(query)

def execute_query(sql):
    global database    
    try:
        cursor = database.cursor()
        cursor.execute(sql)
        database.commit()
        return cursor
    except (AttributeError, MySQLdb.OperationalError):
        try:
            database = MySQLdb.connect(host=db_host, port=db_port,user=db_user,passwd=db_passwd,db=db_structure)
#            max_attempts = 10
#            attempt = 0
#            while ((database.open == 0) and (attempt < max_attempts)):
#                print("Trying to reconnect to DB" + str(attempt))
#                database = MySQLdb.connect(host=db_host, port=db_port,user=db_user,passwd=db_passwd,db=db_structure)
#                attempt += 1
#                if database.open == 0:
#                    time.sleep(1)
#            if database.open == 0:
#                print("Failed to reconnect to database")
#                return "query failed"
            cursor = database.cursor()
            cursor.execute(sql)
            database.commit()
            return cursor
        except MySQLdb.Error, e:
            try:
                print("Failed to write_result Error [%d]: %s" % (e.args[0], e.args[1]))
            except IndexError:
                print("Failed to write_result Error: %s" % str(e))
    return "query failed"
    
def download_image_from_link(link):
    if file_exists(link):
        req = urllb.urlopen(link)
        arr = np.asarray(bytearray(req.read()), dtype=np.uint8)
        image = cv2.imdecode(arr,-1)     
        
        
        #response = requests.get(link)
        #image = Image.open(StringIO(response.content))
        return image
    else:
        print("Failed to get image from url: " + link)
        return "fail"
    
def load_images():
#    http://zulko.github.io/blog/2013/09/27/read-and-write-video-frames-in-python-using-ffmpeg/
    global images
    
    rows_to_fetch = image_buffer*2
    
    rows = get_rows(rows_to_fetch)
    print(type(rows))
    
    for row in rows:
            date = row[1]
            source = row[5] 
            row_id = row[0]
            url = get_path(date, source)
            image_data = download_image_from_link(url)
            if (image_data == "fail"):
                continue
            lock.acquire()
            print("got image data "+str(len(images)))
            try:
                images.append((row_id, image_data))
            finally:
                lock.release()
                

    lock.acquire()
    try:
        images_count = len(images)
    finally:
        lock.release()
    while images_count >= image_buffer:
        lock.acquire()
        try:
            images_count = len(images)
        finally:
            lock.release()
            time.sleep(1)
    rows_to_fetch = image_buffer*2 - images_count
            


def process(name, path, x, y):
#    command1 = [FFMPEG_BIN,'-i', '/home/sergejs/megogo/8dates.mp4', '-']
#    pipe1 = sp.Popen(command1, stdout=sp.PIPE, stderr=sp.PIPE)
#    text  = pipe1.stdout.readline()
#    pipe1.terminate()
#    infos = pipe1.stderr.read()
#    
#    print("TEEEEEEEEEEEEEEXT", infos)
    
    create_graph()
    command = [ FFMPEG_BIN,
            '-i', path+name,
            '-f', 'image2pipe',
            '-pix_fmt', 'rgb24',
            '-vcodec', 'rawvideo', '-']
    pipe = sp.Popen(command, stdout = sp.PIPE, bufsize=10**8)

    global images  

    print("after thread start")
    time = 0
    while(True):
        raw_image = pipe.stdout.read(x*y*3)
        if len(raw_image) != x*y*3:
            pipe.stdout.flush()
            break
        print("RAWWWWWWWW", len(raw_image))
        # transform the byte read into a numpy array
        image = np.fromstring(raw_image, dtype='uint8')
        image = image.reshape((y,x,3))
        # throw away the data in the pipe's buffer.
        pipe.stdout.flush()
        success = True
#        success, image = vidcap.read()
        if success:
#            cv2.imshow("image", image)
#            cv2.waitKey()
#            image_data = grab_frame()
                       
            rez = run_inference_on_image(image)

            print("REZ" + rez)
            write_result(rez ,time, name)
        else:
            print("failed to grab frame")
            time.sleep(1)
        for i in range(24):
            raw_image = pipe.stdout.read(x*y*3)
            pipe.stdout.flush()
            if len(raw_image) != x*y*3:
                break
                break
           
#            print("RAWWWWWWWW1", len(raw_image))
        time+=1
   
def main(_):
    
    maybe_download_and_extract()

#    signal.signal(signal.SIGINT, signal_handler)
#    atexit.register(at_exit)
    path = '/media/apply/F6B4D9C1B4D98491/megogo/'
    names = [('LegokNaPomine.mp4',832,346),('LubovISeksNaIbice',832,466), ('mojParenjPsih.mp4',832,346),('moloda_i_prekrasna.mp4',832,448),('muvi43.mp4',832,468),('oni_bili_soldatami_2.mp4',832,356),('ostrov_vezenija.mp4',832,348),('pompei.mp4',832,348),('privi4ka_rasstavatsja.mp4',832,448),('s_8_marta_muzhchini.mp4',832,468),('spjachaja_krasavica.mp4',832,450),('stalingrad.mp4',832,468),('holostjachki.mp4',832,348)]
    for nam-e in names:
        process(name[0], path, name[1], name[2])       
        print("after processing", name[0])

if __name__ == '__main__':
  tf.app.run()
