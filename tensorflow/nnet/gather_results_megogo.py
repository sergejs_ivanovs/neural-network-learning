# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 10:35:11 2016

@author: rotjix
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import re
import sys
import MySQLdb
import simplejson as json
from collections import deque
import atexit
import signal
#from datetime import datetime
from datetime import datetime as dt, timedelta

#db_host = "localhost"
#db_port = 3306


#db_host = "85.9.214.154"
#db_port = 13306
#db_user = "test"
#db_passwd="test"
#db_structure = "test"


db_host = "localhost"
db_user = "root"
db_passwd="1password!"
db_structure = "test"

#db_host = "localhost"
#db_user = "test"
#db_passwd="test"
#db_structure = "test"

minConfidence = 0.02
treshConfidence = 0.4
secsHist = 15
prevSecs = deque()
prevSecsFallback = deque()
minResults = 2
#lastSec = dt.utcfromtimestamp(0)
lastSec = 0
#sourceId = 3
movie_name = "8dates.mp4"

renamables = {
    "web site, website, internet site, site": "ad, banner, advertisment"
}

blacklist = [
    "potter's wheel",
    "balance beam, beam"
]

currLabels = {}
mysqlTimeF = '%Y-%m-%d %H:%M:%S'

#database = MySQLdb.connect(host=db_host, port=db_port,user=db_user,passwd=db_passwd,db=db_structure)

database = MySQLdb.connect(host=db_host, user=db_user,passwd=db_passwd,db=db_structure)

#images = deque([])
#run_image_loading = True
#image_buffer = 98
#lock = threading.Lock()

def disconnect_from_db():
    database.close()
    
    
    
def get_rows(limit=30):
    global sourceId
    cursor = database.cursor()
    #cursor.execute("SELECT * FROM `ContextTmp` WHERE `processed` = 1 AND sourceId = " + str(sourceId) + " ORDER BY ts ASC LIMIT " + str(limit))
#    cursor.execute("SELECT * FROM `ContextTmp` WHERE `processed` = 1 AND sourceId = " + str(sourceId) + " ORDER BY ts ASC")
    cursor.execute("SELECT * FROM `megogoNeur` WHERE `name` = '" + movie_name + "' ORDER BY ts ASC")
    # AND equalized=0 
    results = cursor.fetchall()
#    query = "UPDATE `ContextTmp` SET `processed`= 3 WHERE id IN ("
#    for row in results:
#        query+= str(row[0])+", "
#    query = query[:-2]
#    query += ")"
#    cursor.execute(query)
#    database.commit()
    return results
    
    
#def setEqualised(id):
#    cursor = database.cursor()
#    query = "UPDATE `ContextTmp` SET `equalized`=1 WHERE id = " +str(id)
#    cursor.execute(query)
#    database.commit()
    
def writeLabelInSecHist(label, labelData):
    global secsHist
    global prevSecs
    global prevSecsFallback
    
    if labelData["confHist"][-1] > 0:
        it = 1
        while it <= len(prevSecsFallback[-1]) and prevSecsFallback[-1][-it][1] < labelData["confHist"][-1]:
            it += 1;       
        #print("adding %s"%label)
        prevSecsFallback[-1].insert(len(prevSecsFallback[-1])-it+1, [label,labelData["confHist"][-1]])
                                
    #print(prevSecsFallback[-1])
                
  #  if labelData["gotGoodConfidence"]:
  #      prevSecs[-1].append([label,labelData["confHist"][-1]]);
    hadConfNotNull = False
    if labelData["gotGoodConfidence"]:
        #print(label + str(labelData["framesPresent"]))
        for i in range(1,labelData["framesPresent"]+1):
            #print(labelData["confHist"])
            hadConfNotNull = labelData["confHist"][-i] > 0
            if not hadConfNotNull:
                continue
            found = False;
            foundIdx = -1;
            for k in range(0,len(prevSecs[-i])):
                if prevSecs[-i][k][0] == label:
                    foundIdx = k
                    found = True
                    break
            #print("Found = " + str(found))
            if found:
                break
            
#            print("----------------")
#            print(prevSecs[-1 - i])
#            print([label,labelData["confHist"][-1 - i]])
            #print("appendin")
            prevSecs[-i].append([label,labelData["confHist"][-i]]);
                
        #for i, tlabel in secsHist[-1]:
        #     if tlabel[0] ==  label            
    
    

    
def rotateSecHist():
#    global sourceId
    global movie_name
    global secsHist
    global prevSecs
    global prevSecsFallback
    #write last sec
    if len(prevSecs) >= secsHist:
        lastSecRowFallback = prevSecsFallback.popleft()
        lastSecRow = prevSecs.popleft()
        
        #add missing records
        if len(lastSecRow) < minResults:
            #remove duplicates
            for li in range(0,len(lastSecRow)):
                fi = 0
                while fi < len(lastSecRowFallback):
                    if lastSecRowFallback[fi][0] == lastSecRow[li][0]:
                        del lastSecRowFallback[fi]
                    else:                            
                        fi+=1
            #insert missing records
            while len(lastSecRow) < minResults and len(lastSecRowFallback) > 0:
                lastSecRow.append(lastSecRowFallback[0])
                del lastSecRowFallback[0]
                
        if len(lastSecRow) > 0:
            lastSecTs = lastSec - secsHist
           # print([lastSecTs,lastSecRow,sourceId]);  
            
            jsonObject = re.sub("((?<![(,])'(?![,)]))", "''", str(json.dumps(lastSecRow)))
            cursor = database.cursor()
            cursor.execute("INSERT INTO ContextTmpOut "
               "(ts, tags, name) "
               "VALUES ('%s', '%s', '%s');"%(lastSecTs,jsonObject,movie_name)
               )
    #add new sec
    prevSecs.append([])
    prevSecsFallback.append([])
    database.commit()

def endOfSec():
    global currLabels
    
    rotateSecHist()
            
    items2Del = []
    for k, label in currLabels.iteritems():
        if len(label["confHist"]) > secsHist:
            currLabels[k]["confHist"].popleft()
        if not label["present"]:
            label["framesPresent"] = label["framesPresent"] - 2
            currLabels[k]["confHist"].append(0)
        label["present"] = False;
        if label["framesPresent"] <= 0:
            items2Del.append(k)
    for it in items2Del:
        del currLabels[it]
        
    for k, label in currLabels.iteritems():
        writeLabelInSecHist(k, label)
        
    #print("------------------------------")
    #print(prevSecs)
           
    
def main_loop():
    global images
    global currLabels
    global lastSec
    
    global renamables 
    global blacklist
    
    global movie_name

    cursor = database.cursor()
    cursor.execute("SET innodb_lock_wait_timeout = 240;")
    #cursor.execute("UPDATE ContextTmp SET equalized = 0 WHERE sourceId = " + str(sourceId) + " ;")
    cursor.execute("DELETE FROM `ContextTmpOut` WHERE `name` = '" + movie_name + "'; ")
    database.commit()  
    
    rows = get_rows()
    rowsProcessed = 0
    
    for row in rows:
        #print("{0} {1}".format(row[1],row[4]))
        print("ROW", row[1], lastSec)
        secDiff = int(row[1]) - lastSec
        
        if not lastSec == 0 and secDiff == 0:
            continue
        
        rowsProcessed = rowsProcessed+1
        if rowsProcessed%60 == 0 :
            print("processed ts %s"%(row[1]))
            
        if not lastSec == 0 and secDiff > 1:
            print("Time gap" + str(secDiff))
            for i in range(1,secDiff):
                endOfSec()
                
        lastSec = row[1]
        #setEqualised(row[0])
            
        if not row[2] :
            endOfSec()
            continue
        
        labels = []
        try:
            labels = json.loads(row[2])
        except ValueError, e:
            endOfSec()
            continue
                        
        for label in labels:
            if label[0] in blacklist:
                continue;   
                
            if label[0] in renamables:
                label[0] = renamables[label[0]];
            
            if float(label[1]) < minConfidence:
                continue
            if not label[0] in currLabels:
                currLabels[label[0]] = {
                    "framesPresent": 1,
                    "firstPresent": row[1],
                    "lastPresent": row[1],
                    "present": True,
                    "gotGoodConfidence": bool(float(label[1])>treshConfidence),
                    "gotGoodConfidenceInit": bool(float(label[1])>treshConfidence),
                    "confHist":deque([float(label[1])])
                }
                #if label[0] == "maze, labyrinth":
                
            else:
                currLabels[label[0]]["gotGoodConfidenceInit"] = False
                currLabels[label[0]]["framesPresent"] = min(secsHist,currLabels[label[0]]["framesPresent"] + 1)
                currLabels[label[0]]["present"] = True
                currLabels[label[0]]["lastPresent"] = row[1]
                if ( not currLabels[label[0]]["gotGoodConfidence"] ) and bool(float(label[1])>treshConfidence):
                    currLabels[label[0]]["gotGoodConfidenceInit"] = True
                currLabels[label[0]]["gotGoodConfidence"] = currLabels[label[0]]["gotGoodConfidence"] or bool(float(label[1])>treshConfidence)
                currLabels[label[0]]["confHist"].append(float(label[1]))
                    
        #print("====================================")
        #print(currLabels)
        endOfSec()
        #print(currLabels)
    
   
    
def signal_handler(signal, frame):
    at_exit()
    sys.exit(0)
    
    # disconnect from server
def at_exit():
    print("At exit called")
#    cursor = database.cursor()
#    query = "UPDATE `ContextTmp` SET `processed`= 0 WHERE id IN ("
#    for id, image_data in images:
#        query+= str(id)+", "
#    query = query[:-2]
#    query += ")"
#    cursor.execute(query)
#    database.commit()
#    disconnect_from_db()
   
def main():
    global movie_name
    signal.signal(signal.SIGINT, signal_handler)
    atexit.register(at_exit, "message")
    
    movie_names = ["8dates.mp4", "supernjanj.mp4", "vojnaBogov.mp4", "vremjaVedm.mp4", "LegokNaPomine.mp4", "mojParenjPsih.mp4", "moloda_i_prekrasna.mp4", "muvi43.mp4", "oni_bili_soldatami_2.mp4", "ostrov_vezenija.mp4", "pompei.mp4", "privi4ka_rasstavatsja.mp4", "s_8_marta_muzhchini.mp4", "spjachaja_krasavica.mp4", "stalingrad.mp4", "holostjachki.mp4"]
    #for mov_name in movie_names:
    movie_name = movie_names[17]        
    main_loop()
    
if __name__ == '__main__':
   main()
   
   

#CREATE TABLE IF NOT EXISTS `ContextTmpOut` (
#`id` int(11) unsigned NOT NULL,
#  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
#  `tags` mediumtext NOT NULL,
#  `sourceId` int(11) NOT NULL,
#  `processed` tinyint(1) NOT NULL DEFAULT '1',
#  `equalized` tinyint(1) NOT NULL DEFAULT '1'
#) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
#
#ALTER TABLE `ContextTmpOut`
# ADD PRIMARY KEY (`id`);
#ALTER TABLE `ContextTmpOut`
#MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
   
   
