# Build the model. Note that we need to make sure the TensorFlow is ready to
# use before this as this command will not build TensorFlow.
bazel build inception/flowers_eval

# Directory where we saved the fine-tuned checkpoint and events files.
TRAIN_DIR=/media/apply/F6B4D9C1B4D98491/image_net/flowers/raw-data/checkpoints/

# Directory where the flowers data resides.
FLOWERS_DATA_DIR=/media/apply/F6B4D9C1B4D98491/image_net/flowers/raw-data/processed/

# Directory where to save the evaluation events files.
EVAL_DIR=/media/apply/F6B4D9C1B4D98491/image_net/flowers/raw-data/eval_rez/


# Directory where to save the evaluation events files.
MODEL_DIR=/media/apply/F6B4D9C1B4D98491/image_net/flowers/raw-data/model/

# Evaluate the fine-tuned model on a hold-out of the flower data set.
bazel-bin/inception/flowers_eval \
  --eval_dir="${EVAL_DIR}" \
  --data_dir="${FLOWERS_DATA_DIR}" \
  --subset=validation \
  --num_examples=500 \
  --checkpoint_dir="${TRAIN_DIR}" \
  --input_queue_memory_factfor=1 \
  --run_once \
  --save_model \
  --model_dir="$MODEL_DIR"
