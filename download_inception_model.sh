# location of where to place the Inception v3 model
DATA_DIR=/media/apply/F6B4D9C1B4D98491/image_net/flowers/raw-data/inception_model/
cd ${DATA_DIR}

# download the Inception v3 model
curl -O http://download.tensorflow.org/models/image/imagenet/inception-v3-2016-03-01.tar.gz
tar xzf inception-v3-2016-03-01.tar.gz
