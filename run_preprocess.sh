FLOWERS_DATA_DIR=/media/apply/F6B4D9C1B4D98491/image_net/flowers/

# build the preprocessing script.
bazel build inception/download_and_preprocess_flowers

# run it
bazel-bin/inception/download_and_preprocess_flowers "${FLOWERS_DATA_DIR}"


cd "${CURRENT_DIR}"
echo "building script: ${WORK_DIR}/build_image_data"
BUILD_SCRIPT="${WORK_DIR}/build_image_data"
OUTPUT_DIRECTORY="${DATA_DIR}"
"${BUILD_SCRIPT}" \

  ./build_image_data \
  --train_directory=/media/apply/F6B4D9C1B4D98491/image_net/flowers/raw-data/train/ \
  --validation_directory=/media/apply/F6B4D9C1B4D98491/image_net/flowers/raw-data/validation/ \
  --output_directory=/media/apply/F6B4D9C1B4D98491/image_net/flowers/raw-data/processed/ \
  --labels_file=/media/apply/F6B4D9C1B4D98491/image_net/flowers/raw-data/labels.txt
