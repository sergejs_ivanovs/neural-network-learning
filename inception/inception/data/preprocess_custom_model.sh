#!/bin/bash
# Copyright 2016 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

# Script to download and preprocess ImageNet Challenge 2012
# training and validation data set.
#
# The final output of this script are sharded TFRecord files containing
# serialized Example protocol buffers. See build_imagenet_data.py for
# details of how the Example protocol buffers contain the ImageNet data.
#
# The final output of this script appears as such:
#
#   data_dir/train-00000-of-01024
#   data_dir/train-00001-of-01024
#    ...
#   data_dir/train-00127-of-01024
#
# and
#
#   data_dir/validation-00000-of-00128
#   data_dir/validation-00001-of-00128
#   ...
#   data_dir/validation-00127-of-00128
#
# Note that this script may take several hours to run to completion. The
# conversion of the ImageNet data to TFRecords alone takes 2-3 hours depending
# on the speed of your machine. Please be patient.
#
# **IMPORTANT**
# To download the raw images, the user must create an account with image-net.org
# and generate a username and access_key. The latter two are required for
# downloading the raw images.
#
# usage:
#  ./preprocess_custom_model.sh [data-dir]
set -e

if [ -z "$1" ]; then
  echo "usage download_and_preprocess_imagenet.sh [data dir]"
  exit
fi

WORK_DIR="$0.runfiles/inception"

LABELS_FILE="/media/apply/videoDisk/new_model/raw_data/labels.txt"

# Note the locations of the train and validation data.
TRAIN_DIRECTORY="/media/apply/videoDisk/new_model/raw_data/train/"
VALIDATION_DIRECTORY="/media/apply/videoDisk/new_model/raw_data/validation/"
DATA_DIR="/media/apply/videoDisk/new_model/"


#extract all

COMPRESSED_DATA="/media/apply/videoDisk/new_model/raw_data/train/"

for tar in "${COMPRESSED_DATA}/"*.tar
do
  dirname=`echo $tar | sed 's/\.tar$//'`
  
  if mkdir $dirname
  then
    if cd $dirname
    then
      echo "unpacking $dirname"
      tar -xf $tar
      cd ..
      # rm -f $zip # Uncomment to delete the original zip file
    else
      echo "Could not unpack $tar - cd failed"
    fi
  else
    echo "Could not unpack $tar - mkdir failed"
  fi
done


# Generate the validation data set.
while read LABEL; do
  VALIDATION_DIR_FOR_LABEL="${VALIDATION_DIRECTORY}${LABEL}"
  TRAIN_DIR_FOR_LABEL="${TRAIN_DIRECTORY}${LABEL}"
  echo "processing label: ${LABEL}"
  # Move the first randomly selected 100 images to the validation set.
  mkdir "${VALIDATION_DIR_FOR_LABEL}"
  echo "creating directory: ${VALIDATION_DIR_FOR_LABEL}"
  VALIDATION_IMAGES=$(ls -1 "${TRAIN_DIR_FOR_LABEL}" | shuf | head -100)
  for IMAGE in ${VALIDATION_IMAGES}; do
    mv -f "${TRAIN_DIRECTORY}${LABEL}/${IMAGE}" "${VALIDATION_DIR_FOR_LABEL}"
  done
done < "${LABELS_FILE}"


