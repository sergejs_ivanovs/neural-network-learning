CUSTOM_DATA_DIR=/media/apply/videoDisk/new_model/
TRAIN_DIR=/raw_data/train/
VAL_DIR=/raw_data/validation/
OUTPUT_DIR=/output/
# build the preprocessing script.
bazel build inception/preprocess_custom_model

# run it
bazel-bin/inception/preprocess_custom_model "${CUSTOM_DATA_DIR}"


cd "${CURRENT_DIR}"
BUILD_SCRIPT="inception/build_image_data"

bazel build "$BUILD_SCRIPT"

echo "building script: inception/build_image_data"
OUTPUT_DIRECTORY="${DATA_DIR}"
"${BUILD_SCRIPT}" \

bazel-bin/inception/build_image_data \
  --train_directory="${CUSTOM_DATA_DIR}${TRAIN_DIR}" \
  --validation_directory="${CUSTOM_DATA_DIR}${VAL_DIR}" \
  --output_directory="${CUSTOM_DATA_DIR}${OUTPUT_DIR}" \
  --labels_file="${CUSTOM_DATA_DIR}/raw_data/labels.txt"


# Build the model. Note that we need to make sure the TensorFlow is ready to
# use before this as this command will not build TensorFlow.
bazel build inception/custom_train

# Path to the downloaded Inception-v3 model.
MODEL_PATH="/media/apply/videoDisk/inception-v3/model.ckpt-157585"

# Directory where to save the checkpoint and events files.
CHECKPOINT_DIR=/media/apply/videoDisk/new_model/checkpoints/

# Run the fine-tuning on the flowers data set starting from the pre-trained
# Imagenet-v3 model.
bazel-bin/inception/custom_train \
  --train_dir="${CHECKPOINT_DIR}" \
  --data_dir="${CUSTOM_DATA_DIR}${OUTPUT_DIR}" \
  --pretrained_model_checkpoint_path="${MODEL_PATH}" \
  --fine_tune=True \
  --initial_learning_rate=0.001 \
  --input_queue_memory_factor=1
